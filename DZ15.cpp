﻿// DZ15.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

//class Example
//{
//private:
//	int a;
//public:
//	int GetA()
//	{
//		return a;
//	}
//	void SetA(int newA)
//	{
//		a = newA;
//	}
//
//};
class Vector
{
public:
	Vector() : x(4), y(4), z(4)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		double dVectorLength = sqrt(x * x + y * y + z * z);
		std::cout << '\n' << x  << ' ' << y << ' ' << z;
		std::cout << "\n" << dVectorLength << "\n";
	}
	int GetA()
			{
				return x;
			}
			void SetA(int newA)
			{
				x = newA;
			}
	
private:
	double x;
	double y;
	double z;
};
int main()
{
	//Example temp;
	//temp.SetA(10);
	//std::cout << temp.GetA() << "\n";
	Vector v;
	v.Show();
	std::cout << "\n" << v.GetA() << "\n";
}